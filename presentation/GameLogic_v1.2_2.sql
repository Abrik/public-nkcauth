SET TERM ^ ;
create or alter procedure SPR_OPV_TEST (
    DIARY_ID integer)
returns (
    D_ID_OUT integer,
    D_INQUIRY integer,
    DIARY_KIND integer,
    DK_OPV_ID integer,
    DK_PO_ID integer,
    DK_RV_ID integer,
    PDS_COST integer)
as
declare variable V_DIARY_KIND integer;
declare variable V_ integer;
begin
  select d.id,dg.id, dg.diary_kind from diary d
  left join diary dg on dg.id = d.inquiry  and d.status =2
  where d.id = :diary_id and d.enabled = 1
  into :d_id_out,:D_INQUIRY,:v_diary_kind;

  select dk.id from diary_kinds dk
  where dk.id = :V_DIARY_KIND and dk.enabled = 1
  into :DIARY_KIND;

  select opv.id, po.id, rv.id,rv.strvalue from  obj_prop_values opv
    inner join property_objects po on po.id = opv.property_object and (po.code like '$PDSB') and po.enabled = 1
    inner join resolved_values rv on rv.id = opv.resolved_value and rv.enabled =1
  where opv.ref = :DIARY_KIND and  opv.enabled = 1 and  upper(opv.tablename) = upper('Diary_kinds')
  into :dk_opv_id,:dk_po_id, :dk_rv_id, :pds_cost;
  suspend;
end
^
commit^


create or alter procedure SPR_OPV_SET_PDSBONUS (
    DIARY_KIND integer,
    GUEST_ID integer,
    DIARY_PO_ID integer)
returns (
    PDS_COST integer)
as
declare variable OPV_PR_RV_PRIORITY integer;
declare variable PDS_CAST_ADD integer;
begin
for
  --! ����� ������� ������
  select
    rv.strvalue
  from  obj_prop_values opv
    inner join property_objects po on po.id = opv.property_object and (po.code like '$PDSB') and po.enabled = 1
    inner join resolved_values rv on rv.id = opv.resolved_value and rv.enabled =1
  where opv.ref = :DIARY_KIND
    and opv.enabled = 1
    and  upper(opv.tablename) = upper('Diary_kinds')
  into :PDS_COST
  do begin
     select first(1) snn(rv.priority) from guests g
         inner join obj_prop_values opv on opv.ref = g.id and  opv.enabled = 1 and  upper(opv.tablename) = upper('GUESTS')
         inner join property_objects po on po.id = opv.property_object  and po.enabled = 1 and po.id =:DIARY_PO_ID
         inner join resolved_values rv on rv.id = opv.resolved_value and rv.enabled = 1
       where g.id = :guest_id
     into :opv_PR_rv_priority;
         if (:opv_PR_rv_priority in (0) ) then PDS_COST = 0;
     --! ����� �������� �  �������� ������
         pds_cast_add = 0;
           if (opv_PR_rv_priority = 2) then pds_cast_add = 1;
           if (opv_PR_rv_priority = 3) then pds_cast_add = 2;
           if (opv_PR_rv_priority = 4) then pds_cast_add = 3;
           if (opv_PR_rv_priority = 5) then pds_cast_add = 5;
           if (opv_PR_rv_priority = 6) then pds_cast_add = 6;
           if (opv_PR_rv_priority = 7) then pds_cast_add = 7;
           if (opv_PR_rv_priority = 8) then pds_cast_add = 8;
           if (opv_PR_rv_priority = 9) then pds_cast_add = 10;
           if (opv_PR_rv_priority = 10) then pds_cast_add = 12;
           if (opv_PR_rv_priority = 11) then pds_cast_add = 15;
           if (opv_PR_rv_priority = 12) then pds_cast_add = 15;
           if (opv_PR_rv_priority = 13) then pds_cast_add = 15;
           if (opv_PR_rv_priority = 14) then pds_cast_add = 15;

         --if (:opv_PDSB_rv_strvalue<0 and :opv_PDSB_rv_strvalue +pds_cast_add>0) then pds_cast_add = -:opv_PDSB_rv_strvalue;

         PDS_COST = PDS_COST + pds_cast_add;
     suspend;
     end
end
^
commit^

create or alter procedure SPR_OPV_SET_GUESTLEVEL_FIRST
returns (
    G_ID integer,
    PO_ID integer,
    RV_ID integer,
    RV_STRVALUE varchar(50),
    PO_CODE varchar(50))
as
begin
  for
    select
       g.id , spr.po_id, spr.rv_id,spr.po_code,spr.rv_strvalue
    from guests g
      left join (select po.id as po_id,po.code as po_code, rv.id as rv_id ,rv.strvalue as rv_strvalue from property_objects po
                 inner join resolved_values rv on rv.property_object = po.id and xupper(rv.strvalue) = xupper('!�������')
                 where (po.code like '$PR__') and po.enabled =1
                ) spr on 1=1
      left join obj_prop_values opv on xupper(opv.tablename) = xupper('Guests')
                                       and opv.ref = g.id
                                       and opv.property_object = spr.po_id
                                       and opv.resolved_value =  spr.rv_id
                                       and opv.enabled =1
    where opv.id is null --and g.id = :guests
  into :g_id,:po_id,:rv_id,:po_code,:rv_strvalue
  do begin
     /*  INSERT INTO OBJ_PROP_VALUES (REF, RESOLVED_VALUE, ENABLED, TABLENAME, PROPERTY_OBJECT, UNCATEGORIZED_VALUE)
              VALUES ( :g_id, :rv_id, 1, 'guests', :po_id, '');
      */

       suspend;
     end


end
^
commit^
create or alter procedure SPR_OPV_SET_GUESTLEVEL
returns (
    DIARY_ID integer,
    GUEST_ID integer,
    OPV_CODE varchar(40),
    OPV_STRVALUE varchar(40),
    DIARY_PO_ID integer,
    PDS_COST integer,
    OPIT integer)
as
declare variable DIARY_COUNT integer;
declare variable GU_STEPM_RV_ID integer;
declare variable STEPM_ID integer;
declare variable STEPM_ID_NEXT integer;
declare variable STEPM_PRIORITY D_INTEGER;
begin
  for
select
  d.id,
  d.guest,
  po.id,
  po.code,
  snn(spr_opv.PDS_COST)
  from diary d
inner join diary dg on dg.id = d.inquiry  and dg.enabled = 1
inner join diary_kinds dk on dk.id = dg.diary_kind and dk.enabled = 1 and (dk.code like '$TS___')
inner join obj_prop_values opv on opv.ref = dk.id
                                  and  opv.enabled = 1
                                  and  upper(opv.tablename) = upper('Diary_kinds')
inner  join property_objects po on po.id = opv.property_object and (po.code like '$PR__') and po.enabled = 1
left join spr_opv_set_pdsbonus(dk.id,d.guest,po.id) spr_opv on 1=1
where 1=1
  and d.kind in (2)
  and d.enabled = 1
  and dg.status = 2
  --and d.id = 63
  --and d.id = 1567
--/*test */  and d.guest = 1119

  into :diary_id,:guest_id,:diary_po_id,:opv_code,:PDS_COST/*:sum_priority1,:sum_priority2,:sum_priority3, :gu_stepm_rv_id*/
  do begin

       ----------------------------------------------------------
       select
         sum(1) diary_count
       from diary d
         inner join diary dg on dg.id = d.inquiry  and dg.enabled = 1
         inner join diary_kinds dk on dk.id = dg.diary_kind and dk.enabled = 1
         inner join obj_prop_values opv on opv.ref = dk.id and  opv.enabled = 1 and  upper(opv.tablename) = upper('Diary_kinds')
         inner join property_objects po on po.id = opv.property_object and po.enabled = 1 and po.id = :diary_po_id
         left join resolved_values rv on rv.id = opv.resolved_value and  rv.enabled = 1
       where 1=1
         and d.kind in (2)
         and d.enabled = 1
         and dg.status = 2
         and d.guest = :guest_id
       into :diary_count;
       ----------------------------------------------------------
       stepm_priority = :diary_count;

    /*    if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;
        if (sum_priority = 1) then  stepm_priority =9;  */


   select rv.strvalue from  property_objects po
     left join resolved_values rv on rv.property_object = po.id and  rv.enabled = 1
   where po.id = :diary_po_id
     and rv.priority = :stepm_priority
   into :opv_strvalue ;

   select cast(snn(spr.strvalue)as integer) from spr_getobjpropvalue ('GUESTS',:GUEST_ID,'$OPIT','')spr into :opit;
   opit = opit +1;
  suspend;

  end

end
^
commit^
create or alter procedure SPR_OPV_PASPORT_OPIT (
    GUEST integer)
returns (
    OPV_OPIT_VALUE integer,
    LAVEL_STEP integer,
    END_STEP integer,
    STEP1 integer,
    STEP2 integer,
    STEP3 integer,
    STEP4 integer,
    STEP5 integer,
    STEP6 integer,
    STEP7 integer,
    STEP8 integer,
    STEP9 integer,
    STEP10 integer,
    STEP11 integer,
    STEP12 integer,
    STEP13 integer,
    STEP14 integer,
    STEP15 integer,
    STEP16 integer,
    STEP17 integer,
    STEP18 integer,
    STEP19 integer,
    STEP20 integer,
    STEP21 integer,
    STEP22 integer,
    STEP23 integer,
    STEP24 integer,
    STEP25 integer,
    KIND integer,
    NEW_PARAM integer)
as
declare variable RV_PRIORITY integer;
declare variable PO_ID integer;
declare variable COUNT_ALL integer;
declare variable I integer;
declare variable LAVEL_STEP_VALUE_MAX integer;
declare variable BEFORE_LAVEL_STEP_VALUE_MAX integer;
declare variable K integer;
begin
  select cast(snn(spr.strvalue)as integer) as opv_opit_value
    from spr_getobjpropvalue ('GUESTS',:GUEST,'$OPIT','')spr
  into :opv_opit_value;

  end_step =3;
  LAVEL_STEP = 1;
  LAVEL_STEP_VALUE_MAX = 3;
  Before_LAVEL_STEP_VALUE_MAX = 0;

  count_all = 0;
  i =0;

  while (:count_all < 322) do
  begin
    count_all = count_all + 1;
    i = i+1;
    --tr = opv_opit_value
    k = 1;
      if (k <= :end_step) then
        if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then
          kind = 1;
        else
          kind = 0;
      else  kind = -1;
    step1 = kind;
 --   k = 2; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step2 = kind;

k = 1; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step1 = kind;
k = 2; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step2 = kind;
k = 3; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step3 = kind;
k = 4; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step4 = kind;
k = 5; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step5 = kind;
k = 6; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step6 = kind;
k = 7; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step7 = kind;
k = 8; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step8 = kind;
k = 9; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step9 = kind;
k = 10; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step10 = kind;
k = 11; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step11 = kind;
k = 12; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step12 = kind;
k = 13; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step13 = kind;
k = 14; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step14 = kind;
k = 15; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step15 = kind;
k = 16; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step16 = kind;
k = 17; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step17 = kind;
k = 18; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step18 = kind;
k = 19; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step19 = kind;
k = 20; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step20 = kind;
k = 21; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step21 = kind;
k = 22; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step22 = kind;
k = 23; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step23 = kind;
k = 24; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step24 = kind;
k = 25; if (k <= :end_step) then  if (:opv_opit_value - Before_LAVEL_STEP_VALUE_MAX - (k-1)> 0 ) then kind = 1; else  kind = 0;  else  kind = -1; step25 = kind;



    if (i = end_step)
      then
        begin
          suspend;
          i = 0;
          end_step = end_step +1;
          Before_LAVEL_STEP_VALUE_MAX = Before_LAVEL_STEP_VALUE_MAX + LAVEL_STEP_VALUE_MAX;
          LAVEL_STEP = LAVEL_STEP +1;
          LAVEL_STEP_VALUE_MAX = LAVEL_STEP_VALUE_MAX + 1;
          if (LAVEL_STEP =24) then break;
        end
  end
  --if (i <> end_step) then suspend;
end
^
commit^
create or alter procedure SPR_OPV_PASPORT_ALL (
    GUEST integer)
returns (
    OK_NAME D_LARGESTR,
    PO_NAME D_LARGESTR,
    OK_STEP_COUNT integer,
    PO_STEP_COUNT integer)
as
declare variable PO_ID integer;
declare variable OK_ID integer;
begin
  for
    select
      ok.id, ok.name as ok_name
    from opv_kind ok
    where ok.name <> 'xxx'
    order by ok.priority
  into :ok_id,:ok_name do
  begin
    --===================================================================
    OK_STEP_COUNT = 1000;
    for
      select po.name,snn(rv.priority) as priority from opv_opv oo
        left join property_objects po on po.id = oo.po_id
        left join obj_prop_values opv on opv.property_object = po.id and opv.ref = :guest
        left join resolved_values rv on rv.id = opv.resolved_value
      where oo.kindid = :ok_id
        and oo.enabled = 1

    into :po_name, :PO_STEP_COUNT do
    begin
      if (:PO_STEP_COUNT < :OK_STEP_COUNT) then
        OK_STEP_COUNT = :PO_STEP_COUNT;
    end
    --===================================================================
    for
      select po.name,snn(rv.priority) as priority from opv_opv oo
        left join property_objects po on po.id = oo.po_id
        left join obj_prop_values opv on opv.property_object = po.id and opv.ref = :guest
        left join resolved_values rv on rv.id = opv.resolved_value
      where oo.kindid = :ok_id
        and oo.enabled = 1
    into :po_name, :PO_STEP_COUNT do
    begin
      suspend;
    end
    --===================================================================
  end
end
^
commit^
create or alter procedure SPR_OPV_PASPORT_1 (
    GUEST integer)
returns (
    OPV_KIND_NAME D_LARGESTR,
    PO_NAME D_LARGESTR,
    STEP_COUNT integer,
    STEP1 integer,
    STEP2 integer,
    STEP3 integer,
    STEP4 integer,
    STEP5 integer,
    STEP6 integer,
    STEP7 integer,
    STEP8 integer,
    STEP9 integer)
as
declare variable RV_PRIORITY integer;
declare variable PO_ID integer;
begin
  for
    select
      ok.name as ok_name, po.id,po.name
    from opv_opv oo
      left join opv_kind ok on ok.id = oo.kindid
      left join property_objects po on po.id = oo.po_id
    where oo.enabled =1
    order by ok.priority,ok.name,po.name
  into :opv_kind_name, :po_id,po_name
  do begin
       step1 = 0;
       step2 = 0;
       step3 = 0;
       step4 = 0;
       step5 = 0;
       step6 = 0;
       step7 = 0;
       step8 = 0;
       step9 = 0;
       STEP_COUNT = 0;
       rv_priority = -1;
       select rv.priority from obj_prop_values opv
         inner join property_objects po on po.id = opv.property_object and po.enabled = 1 and po.id = :po_id
         inner join resolved_values rv on rv.id = opv.resolved_value
       where opv.enabled = 1
         and xupper(opv.tablename) = xupper('GUESTS')
         and opv.ref = :guest
       into :rv_priority;

       if (:rv_priority>=1) then
       begin
         step1 =1;
         STEP_COUNT = 1;
         if (:rv_priority>=2) then
         begin
           step2 =1;
           STEP_COUNT = 2;
           if (:rv_priority>=3) then
           begin
             step3 =1;
             STEP_COUNT = 3;
             if (:rv_priority>=4) then
             begin
               step4 =1;
               STEP_COUNT = 4;
               if (:rv_priority>=5) then
               begin
                 step5 =1;
                 STEP_COUNT = 5;
                 if (:rv_priority>=6) then
                 begin
                   step6 =1;
                   STEP_COUNT = 6;
                   if (:rv_priority>=7) then
                   begin
                     step7 =1;
                     STEP_COUNT = 7;
                     if (:rv_priority>=8) then
                     begin
                       step8 =1;
                       STEP_COUNT = 8;
                       if (:rv_priority>=9) then
                       begin
                         STEP_COUNT = 9;
                         step9 =1;
                       end
                     end
                   end
                 end
               end
             end
           end
         end
       end


       suspend;
     end

end
^
commit^

create or alter procedure SPR_OPV_INSERT_TS_OPV_PR (
    ID_DK integer,
    PR_PO_ID integer,
    TS_PRIORITY integer)
as
declare variable TS_RV_ID integer;
begin
  for select first (1) rv.id from property_objects po
  inner join resolved_values rv on rv.property_object = po.id and rv.enabled = 1
  where po.enabled =1 and po.id = :pr_po_id and rv.priority = :ts_priority
  into :ts_rv_id  do
  begin
    INSERT INTO OBJ_PROP_VALUES (REF, RESOLVED_VALUE, ENABLED, TABLENAME, PROPERTY_OBJECT, UNCATEGORIZED_VALUE)
      VALUES ( :ID_DK, :ts_rv_id, 1, 'Diary_kinds', :PR_PO_ID, '');
  end
end
^
commit^
create or alter procedure SPR_OPV_INSERT_TS_OPV_PDS (
    ID_DK integer,
    PDS_VALUE varchar(10))
as
declare variable PDS_VALUE_RV_ID integer;
declare variable PDS_PO_ID integer;
begin
  select first (1) rv.id from property_objects po
  inner join resolved_values rv on rv.property_object = po.id and rv.enabled = 1
  where po.enabled =1 and upper(po.code) = upper('$PDSB')
  and upper(rv.strvalue) = upper(:pds_value)
  into :pds_value_rv_id ;

  select po.id from property_objects po
  where upper(po.code) = upper ('$PDSB') and po.enabled = 1
  into :pds_po_id;
  
INSERT INTO OBJ_PROP_VALUES (REF, RESOLVED_VALUE, ENABLED, TABLENAME, PROPERTY_OBJECT, UNCATEGORIZED_VALUE)
  VALUES ( :ID_DK, :pds_value_rv_id, 1, 'Diary_kinds', :pds_po_id, '');
end
^
commit^

create or alter procedure SPR_OPV_INSERT_TS (
    PO_ID integer,
    TS_ID integer,
    NAME D_LARGESTR)
returns (
    ID_DK integer)
as
declare variable CODE D_CODE10;
begin
  code = '$TS';
  if (:po_id<10) then code = code||'0'||:po_id; else code = code||''||:po_id ;
  code = code||:ts_id ;
  id_dk = gen_id(g_diary_kinds,1);
  INSERT INTO DIARY_KINDS (id,CODE, NAME, ENABLED, PRERECORD_OPERATION, PRERECORD_SUMMA, DEFAULT_DURATION, DEFAULT_Q_ADULT, DEFAULT_Q_CHILD, ALLOWABLE_DELAY, USEOBJPROP, USEROOM, USEQUICKCREATION, USEAUTOFILLGUESTBYQUICKPANEL)
    VALUES (:id_dk,:CODE, :NAME, 1, 0, 0, 0.013888888889, 6, 0, 0.0062499999985, 0, 1, 0, 1);
  suspend;
end
^
commit^
create or alter procedure SPR_OPV_INSERT_RULES (
    CODE D_CODE,
    NAME D_LARGESTR,
    PRIORITY integer,
    CONDITIONS D_1KMEMO,
    CONDITIONLIST D_1KMEMO)
as
declare variable RFC_ID integer;
begin
RFC_ID = gen_id(G_RULES_FOR_CONDITIONS,1);
INSERT INTO RULES_FOR_CONDITIONS (id,CODE, NAME, CONDITIONS, PARAMBLOB, ENABLED, PARAMSTR, ISACTIVE, KIND, PRIORITY) VALUES (:RFC_ID, :code, :name, :CONDITIONS , NULL, 1, :CONDITIONLIST, 1, 1, :priority);

--INSERT INTO RULES_FOR_CONDITIONS (ID, CODE, NAME, CONDITIONS, PARAMBLOB, ENABLED, PARAMSTR, ISACTIVE, KIND, PRIORITY) VALUES (:RFC_ID, '1', '1', '(:priority_prop_obj67$ >= 1)', NULL, 1, '&ConditionList=:priority_prop_obj67$', 1, 1, 0);
  suspend;
end
^
commit^

create or alter procedure SPR_OPV_INSERT_ROOM_KIND (
    CODE D_CODE,
    NAME D_LARGESTR,
    RESOURCE_CATEG_ID integer,
    BEDS integer)
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR,
    RETURN_ID integer)
as
declare variable ROOM_KINDS_ID integer;
begin
  step = '�������: ��� �������'; STEP_INFO =:NAME; MSG = 'Error';
  room_kinds_id = 0;
  if (RESOURCE_CATEG_ID<>0 and :BEDS<>0 and :NAME<>'' and :CODE<>'') then
  begin
    select rk.id from room_kinds rk where rk.code = :CODE and rk.enabled = 1 into :room_kinds_id;
    if (:room_kinds_id = 0) then
      begin
        room_kinds_id = GEN_ID(G_ROOM_KINDS_ID,1);
        INSERT INTO ROOM_KINDS (ID, HOTEL, CODE, NAME, NAME2, AMOUNTOFROOMS, RATE, INFO, COLOR, BEDS, ENABLED, RESOURCE_CATEG, FACTOR3, SYNCID, VERSID, PARAMSTR, TOTALPLACES, OPERATION, CHECKABONEMENT, ISPROKAT, KINDOFSHOWINSCHEDULE, RULEATTENDANCECONTROL, KIND, HOTELORG)
        VALUES (:room_kinds_id, 0, :CODE, :NAME, '', 64, 0, '', 16777215, :BEDS, 1, :resource_categ_id, 0, 124, 0, '&abonement_check=1&isprokat=0', 107200, 0, 1, 0, 2, 0, 0, 0);
        MSG = 'ok';
      end
    else
      MSG = 'No. Dublicate';
  end
  return_id = room_kinds_id;
  suspend;
end
^
commit^
create or alter procedure SPR_OPV_INSERT_ROOM (
    ROOMKIND integer,
    BUILDING_CODE D_CODE,
    floor D_TINYSTR,
    NUMBER D_CODE,
    QPLACES integer,
    NAME D_MEDIUMSTR,
    PARAMSTR D_1KMEMO)
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR,
    RETURN_ID integer)
as
declare variable ROOMID integer;
declare variable BUILD_ID integer;
begin
  step = '�������: ������'; STEP_INFO =:NAME; MSG = 'Error';
  roomid = 0;
  if (ROOMKIND<>0 and :BUILDING_code<>'' and :FLOOR<>'' and :NUMBER<>'' and :NAME<>'') then
  begin
    BUILD_id=0;
    select b.id from building b where b.code = :BUILDING_code into :BUILD_id;
    if (:BUILD_id>0) then
    begin
      select r.id from rooms r where r.number = :number and r.enabled = 1 into :roomid;
      if (roomid = 0) then
        begin
        ROOMID=GEN_ID(G_ROOMS_ID,1);
        INSERT INTO ROOMS(id,ROOMKIND,ENABLED,BUILDING,FLOOR,NUMBER,QPLACES,NAME,PARAMSTR,ROOMPARENT)
        VALUES(:roomid,:ROOMKIND,1,:BUILD_id,:FLOOR,:NUMBER,:QPLACES,:NAME,'&LIGHTCONTROLNUM=&UseGKHOSTConnect=0',0);
        MSG = 'ok';
        end
      else
        MSG = 'No. Dublicate';
    end
    else
        MSG = 'error: Build_id';
  end
  return_id = roomid;
  suspend;
end
^
commit^
create or alter procedure SPR_OPV_INSERT_RESOURCE_CATEG (
    CODE D_CODE,
    NAME D_LARGESTR,
    KIND integer)
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR,
    RETURN_ID integer)
as
declare variable RESOURCE_CATEG_ID integer;
begin
  step = '�������: ��������� �������'; STEP_INFO =:NAME; MSG = 'error';
  resource_categ_id =0;
  select rc.id from resource_categ  rc  where rc.code = :CODE and rc.enabled = 1 into :resource_categ_id;
  if (:resource_categ_id = 0) then
    begin
    resource_categ_id = GEN_ID(G_RESOURCE_CATEG_ID,1);
    INSERT INTO RESOURCE_CATEG (ID, CODE, NAME, INFO, KIND, FACTOR3, ENABLED, KIND2) VALUES (:resource_categ_id, :code, :name, NULL, :kind, 0, 1, 1);
    MSG = 'ok';
    end
  else
    MSG = 'No. Dublicate';
  return_id = resource_categ_id;
  suspend;
end
^
commit^
create or alter procedure SPR_OPV_INSERT_PRIZ (
    OPV_OPIT D_LARGESTR,
    CODE D_CODE,
    NAME varchar(20))
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR)
as
declare variable PO_ID integer;
declare variable RV_ID integer;
declare variable RESOURCE_CATEG_ID integer;
declare variable ROOM_KINDS_ID integer;
declare variable ROOM_ID integer;
declare variable RETURN_ID integer;
declare variable BUILD_ID integer;
declare variable OPV_OPIT_SUCCESS integer;
declare variable ERROR integer;
declare variable ERROR_PARAM D_LARGESTR;
begin

  select first(1) po.id, rv.id from property_objects po
  inner join resolved_values rv on rv.priority = :OPV_OPIT
  where po.code = '$OPIT'
  into :po_id, :rv_id ;

  if (po_id >0 and (rv_id>0)) then
    begin
      select first(1) rc.id, rk.id, r.id from resource_categ rc
        left join room_kinds rk on rk.resource_categ = rc.id and rk.code = :code
        left join rooms r on r.roomkind = rk.id and r.number = :CODE
        where rc.code = '�����'
      into :resource_categ_id, :room_kinds_id, :room_id;
      --=========================================================
      resource_categ_id = snn(resource_categ_id);
      room_kinds_id = snn(room_kinds_id);
      room_id = snn(room_id);
      if (:resource_categ_id = 0) then
      begin
        execute procedure spr_opv_insert_resource_categ('�����','��������',0)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        resource_categ_id = :return_id;
      end
      if (:room_kinds_id = 0) then
      begin
        execute procedure SPR_OPV_INSERT_ROOM_KIND     (:code,:name,:resource_categ_id,1000)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        room_kinds_id= :return_id;
      end
        execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','1',:code,1000,:name,'')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        room_id = :return_id;
        if (room_id>0) then
          begin
            select success from  SPR_SETOBJPROPVALUE('rooms', :room_id,'$OPIT', :OPV_OPIT,'', '')
            into :OPV_OPIT_SUCCESS;
            if (:OPV_OPIT_SUCCESS = 0) then
              begin
                msg = 'error.�������� ������� �� ����������';
                error = 301001;  error_param = ' ��������: ����. ��������:'||:OPV_OPIT;
                execute procedure SYS$EXCEPTION( :error, '������ ��������� �������� �������. �������: �������.'||error_param, '' );

              end
           end

      --=========================================================
    end
  else
    begin
      step = '�������� �������'; STEP_INFO ='$OPIT [��������:'||:OPV_OPIT||']. ��������� '; MSG = 'Error';
      suspend;
    end
end
^
commit^

create or alter procedure SPR_OPV_INSERT_PR_1 (
    PO_PR_NUMBER_INT integer,
    PO_PR_NAME D_LARGESTR)
returns (
    PR_PO_ID integer,
    PO_NUMBER varchar(100))
as
declare variable COUNT_PO_PR varchar(100);
begin
  if (:PO_PR_NUMBER_INT<10) then
    PO_NUMBER = '0'||PO_PR_NUMBER_INT; else PO_NUMBER = ''||:PO_PR_NUMBER_INT ;
  if (PO_PR_NAME = '') then
    PO_PR_NAME = '��������� �'||:PO_NUMBER;

  select count(*) count_po_pr from property_objects po where xupper(po.name) = xupper(:PO_PR_NAME) and po.enabled =1  into :count_po_pr;
  if (:count_po_pr =0 ) then
    begin
      PR_PO_ID = gen_id(g_Property_objects,1);
      insert into property_objects (id,code, name,enabled, paramstr,checkunique,CHECK_RIGHT_TO_MODIFY,USEPRIORITY)
             values (:PR_PO_ID,'$PR'||:PO_NUMBER,:PO_PR_NAME,1,'&tablenames=GUESTS|DIARY_KINDS',0,1,1);
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '!�������',             1   , 0, '$PR'||:PO_NUMBER||'_99');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 1� �������',     1, 1, '$PR'||:PO_NUMBER||'_01');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 2� �������',     1, 2, '$PR'||:PO_NUMBER||'_02');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 3� �������',      1, 3, '$PR'||:PO_NUMBER||'_03');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 4� �������',       1, 4, '$PR'||:PO_NUMBER||'_04');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 5� �������',       1, 5, '$PR'||:PO_NUMBER||'_05');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 6� �������',       1, 6, '$PR'||:PO_NUMBER||'_06');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 7� �������',       1, 7, '$PR'||:PO_NUMBER||'_07');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 8� �������',       1, 8, '$PR'||:PO_NUMBER||'_08');
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PR_PO_ID, '����� 9� �������',      1, 9, '$PR'||:PO_NUMBER||'_09');
    suspend;
    end
  else
    begin
      PR_PO_ID = (select first (1) po.id  from property_objects po where xupper(po.name) = xupper(:PO_PR_NAME)  and po.enabled =1 );
      suspend;
    end
end
^
commit^
create or alter procedure SPR_OPV_INSERT_PR_3 (
    I_PO integer,
    NAME D_LARGESTR,
    NAME_ADD D_LARGESTR,
    PDS_VALUE integer)
as
declare variable NAME_ADD1 varchar(10);
declare variable NAME_ADD2 varchar(10);
declare variable NAME_ADD3 varchar(10);
declare variable TS_PRIORITY0 integer;
declare variable TS_PRIORITY1 integer;
declare variable TS_PRIORITY8 integer;
declare variable I_TS integer;
declare variable PR_PO_ID integer;
declare variable ID_DK integer;
declare variable PO_NUMBER varchar(2);
declare variable CONDITIONS D_1KMEMO;
declare variable CONDITIONLIST D_1KMEMO;
begin
 /* name_add1 = '[��] ';   name_add2 = '[����] ';   name_add3 = '[���] ';
  TS_priority0 = 0;     TS_priority1 = 1;        TS_priority8 = 8;

  */
  TS_priority0 = 0; 
  i_ts = 0;
                 --pr_po_id = (select PR_PO_ID from SPR_OPV_INSERT_PR_1(:i_po,:name));-- returning_values (:pr_po_id);
                 EXECUTE PROCEDURE SPR_OPV_INSERT_PR_1(:i_po,:name) returning_values (:pr_po_id,:PO_NUMBER);
                 EXECUTE PROCEDURE SPR_OPV_INSERT_RULES('$RC'||:PO_NUMBER||'1',:name||'(������)' ,1,'(:priority_prop_obj'||:PR_PO_ID||'$ >=3)','&ConditionList=:priority_prop_obj'||:PR_PO_ID||'$');
                 EXECUTE PROCEDURE SPR_OPV_INSERT_RULES('$RC'||:PO_NUMBER||'2',:name||'(�������)',2,'(:priority_prop_obj'||:PR_PO_ID||'$ >=6)','&ConditionList=:priority_prop_obj'||:PR_PO_ID||'$');
                 EXECUTE PROCEDURE SPR_OPV_INSERT_RULES('$RC'||:PO_NUMBER||'3',:name||'(������)' ,3,'(:priority_prop_obj'||:PR_PO_ID||'$ >=9)','&ConditionList=:priority_prop_obj'||:PR_PO_ID||'$');

                 /*if (:isNewGroupName = 0) then
                   begin
                     CONDITIONS = CONDITIONS + ','||:PR_PO_ID;
                     --'&ConditionList=:priority_prop_obj276$|:priority_prop_obj277$'
                    -- '(:priority_prop_obj276$ >= 1) AND (:priority_prop_obj277$ >= 1)'
                   end
                 else
                   begin
                     CONDITIONS = :PR_PO_ID;
                   end */


  if (NAME_ADD<>'') then NAME = NAME||'('||NAME_ADD||')';
  i_ts = i_ts+1; EXECUTE PROCEDURE SPR_OPV_INSERT_TS (i_po,i_ts,name||'(�������� 1)')returning_values (:ID_DK);
                 EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PR (:ID_DK,:pr_po_id,:TS_PRIORITY0);
  --               EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PDS (:ID_DK,:pds_value);
  i_ts = i_ts+1; EXECUTE PROCEDURE SPR_OPV_INSERT_TS (i_po,i_ts, name||'(�������� 2)') returning_values (:ID_DK);
                -- EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PDS (:ID_DK,:pds_value);
                EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PR (:ID_DK,:pr_po_id,:TS_PRIORITY0);
  i_ts = i_ts+1; EXECUTE PROCEDURE SPR_OPV_INSERT_TS (i_po,i_ts, name||'(�������� 3)') returning_values (:ID_DK);
                -- EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PDS (:ID_DK,:pds_value);
                 EXECUTE PROCEDURE SPR_OPV_INSERT_TS_OPV_PR (:ID_DK,:pr_po_id,:TS_priority0);

end
^
commit^
create or alter procedure SPR_OPV_INSERT_PDS
as
declare variable ID_PO integer;
begin
  id_po = gen_id(g_Property_objects,1);
   INSERT INTO PROPERTY_OBJECTS (ID, CODE, NAME, ENABLED, PARAMSTR, CHECKUNIQUE) VALUES (:id_po,'$PDSB', '������� ��� �����', 1, '&tablenames=DIARY_KINDS', 0);
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '0', 1, 1, '$PDSB_1_00');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '5', 1, 1, '$PDSB_1_05');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '10', 1, 2, '$PDSB_1_10');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '15', 1, 2, '$PDSB_1_15');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '20', 1, 3, '$PDSB_1_20');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '-20', 1, 4, '$PDSB_0_20');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '-25', 1, 5, '$PDSB_0_25');
   INSERT INTO RESOLVED_VALUES (PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:id_po, '-30', 1, 6, '$PDSB_0_30');
  suspend;
end
^
commit^
create or alter procedure SPR_OPV_INSERT_KVEST (
    CODE D_CODE,
    NAME varchar(20))
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR)
as
declare variable PO_ID integer;
declare variable RV_ID integer;
declare variable RESOURCE_CATEG_ID integer;
declare variable ROOM_KINDS_ID integer;
declare variable ROOM_ID integer;
declare variable RETURN_ID integer;
declare variable BUILD_ID integer;
declare variable OPV_OPIT_SUCCESS integer;
declare variable ERROR integer;
declare variable ERROR_PARAM D_LARGESTR;
begin
  select first(1) rc.id, rk.id, r.id from resource_categ rc
        left join room_kinds rk on rk.resource_categ = rc.id and rk.code = :CODE
        left join rooms r on r.roomkind = rk.id and r.number = :CODE
        where rc.code = '������'
      into :resource_categ_id, :room_kinds_id, :room_id;
      --=========================================================
      resource_categ_id = snn(resource_categ_id);
      room_kinds_id = snn(room_kinds_id);
      room_id = snn(room_id);
      if (:resource_categ_id = 0) then
      begin
        execute procedure spr_opv_insert_resource_categ('������','������� ��� �������',0)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        resource_categ_id = :return_id;
      end
      if (:room_kinds_id = 0) then
      begin
        execute procedure SPR_OPV_INSERT_ROOM_KIND     (:code,:name,:resource_categ_id,1000)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        room_kinds_id= :return_id;
      end
        execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','1',:code,1000,:name,'')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
        room_id = :return_id;
      --=========================================================

end
^
commit^
create or alter procedure SPR_OPV_GET_RULES (
    GUEST integer,
    RULE_CODE D_CODE)
returns (
    SUCCESS integer)
as
declare variable PAR varchar(1024);
declare variable PRIORITY integer;
declare variable POID integer;
declare variable RULE_ID integer;
begin
  for select po.id, max(rv.priority) from
    property_objects po
    left join spr_opv_get_opv('GUESTS',:guest,po.id,'') s on 1=1
    left join RESOLVED_VALUES rv on rv.id = s.resolved_value
    where rv.id is not null
      and po.enabled=1
      --and xupper(po.code) = xupper (:po_code)
    group by 1
    into :poid, :priority do
    begin
      par = setnamedparam(par,':priority_prop_obj'||:poid||'$',:priority);
    end
  -- get rule id ------------------------------------------------------
  RULE_ID =0;
  select rfc.id from rules_for_conditions rfc where xupper(rfc.code) = xupper (:rule_code) into :RULE_ID;
  SUCCESS = 0;
  select SUCCESS from SP_CHECK_RULES_FOR_CONDITIONS(:RULE_ID,1,:par) into :SUCCESS;
  suspend;
end^
commit^
create or alter procedure SPR_OPV_GET_OPV (
    TABLENAME varchar(31),
    REF integer,
    PROPERTY_ID integer,
    PARAMSTR varchar(1024))
returns (
    STRVALUE varchar(50),
    UNCATEGORIZED_VALUE varchar(50),
    CODE varchar(10),
    RESOLVED_VALUE integer)
as
begin
  tablename=xupper(tablename);
  select first(1) rv.strvalue, opv.UNCATEGORIZED_VALUE, rv.code, rv.id from  obj_prop_values opv
    inner join resolved_values rv on rv.id=opv.resolved_value
    where opv.enabled=1
      and opv.property_object=:PROPERTY_ID
      and xupper(opv.tablename)=:tablename and opv.ref=:ref
    into :strvalue, :UNCATEGORIZED_VALUE, :CODE, :resolved_value;
  suspend;
end^
commit^



create or alter procedure SPR_OPV_CREATE_ROOMS3
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR)
as
begin
  for select spr.step, spr.step_info, spr.msg from spr_opv_insert_kvest('�����1','�����1')  spr  into :step,:step_info, :msg do begin  suspend;  end
  for select spr.step, spr.step_info, spr.msg from spr_opv_insert_kvest('�����2','�����2')  spr  into :step,:step_info, :msg do begin  suspend;  end
  for select spr.step, spr.step_info, spr.msg from spr_opv_insert_kvest('�����3','�����3')  spr  into :step,:step_info, :msg do begin  suspend;  end

end^
commit^
create or alter procedure SPR_OPV_CREATE_ROOMS2
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR)
as
begin
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ('4','��_01','������ �1')  spr  into :step,:step_info, :msg do begin  suspend;  end
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(4,'������','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(4,'������','�������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(12,'��_02','������ �2')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(21,'��_03','������ �3')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(31,'��_04','������ �4')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(42,'��_05','������ �5')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(42,'�����','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(42,'������','������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(54,'��_06','������ �6')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(67,'��_07','������ �7')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(81,'��_08','������ �8')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(81,'���','���')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(81,'������','�������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(97,'��_09','������ �9')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(114,'��_10','������ �10')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(114,'���','���')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(114,'������','������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(133,'��_11','������ �11')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(154,'��_12','������ �12')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(154,'�����','�����')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(154,'������','������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(176,'��_13','������ �13')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
/*for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(201,'��_14','������ �14')  spr  into :step,:step_info, :msg do begin  suspend;  end
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(201,'�����','�����')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(201,'����','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(228,'��_15','������ �15')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(257,'��_16','������ �16')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(257,'�����','�����')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(257,'�����','�������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(289,'��_17','������ �17')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(289,'����','����')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(289,'������','�������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(324,'��_18','������ �18')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(324,'������','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(324,'������','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(362,'��_19','������ �19')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(362,'������','������')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(362,'�����','�����')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(404,'��_20','������ �20')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(404,'������ ','������ ')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(404,'������','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end
        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(450,'��_21','������ �21')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(450,'������','���������')  spr  into :step,:step_info, :msg do begin  suspend;  end        
for select spr.step, spr.step_info, spr.msg from SPR_OPV_INSERT_PRIZ(450,'������','��������')  spr  into :step,:step_info, :msg do begin  suspend;  end
*/
end^
commit^
create or alter procedure SPR_OPV_CREATE_ROOMS
returns (
    STEP D_LARGESTR,
    STEP_INFO D_LARGESTR,
    MSG D_LARGESTR)
as
declare variable ROOMID integer;
declare variable ROOM_KINDS_ID integer;
declare variable RESOURCE_CATEG_ID integer;
declare variable BUILD_ID integer;
declare variable RETURN_ID integer;
declare variable RV_ID integer;
declare variable PO_ID integer;
begin
resource_categ_id = 0;
--== Create resource_categ  '�����' ==============================================================================================
    execute procedure spr_opv_insert_resource_categ('�����','��������',0)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    resource_categ_id = return_id;
    execute procedure SPR_OPV_INSERT_ROOM_KIND     ('�����','�����',:resource_categ_id,1000)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    room_kinds_id = return_id;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'��','1','�����',10000,'�����','') returning_values (:step,:STEP_INFO,:msg,:return_id);suspend;
    for
      select first (1) po.id, rv.id from property_objects po
        inner join resolved_values rv on rv.property_object = po.id and rv.enabled = 1
      where po.enabled =1 and po.code = '$ISCEN' and rv.code = '$ISCEN2'
    into :PO_ID, :rv_id  do
    begin
    INSERT INTO OBJ_PROP_VALUES (REF, RESOLVED_VALUE, ENABLED, TABLENAME, PROPERTY_OBJECT, UNCATEGORIZED_VALUE)
      VALUES ( :return_id, :rv_id, 1, 'ROOMS', :PO_ID, '');
    end
--================================================================================================
--== Create room_kinds  '�����' [resource_categ  '�����']=========================================
    execute procedure spr_opv_insert_resource_categ('�����','���������',0)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    resource_categ_id = return_id;
    execute procedure SPR_OPV_INSERT_ROOM_KIND('������','���������(����������)',:resource_categ_id,1000)returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    room_kinds_id = return_id;
--================================================================================================
step = '�������: �������'; STEP_INFO ='���� �������: ������'; MSG = 'error';
if (room_kinds_id >0) then
  begin
        execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','1','���',1000,'�������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','2','�����',1000,'������ ����','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','3','����',1000,'����������� ����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','4','�����',1000,'����������� ������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','5','����',1000,'��������� ���������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','6','���',1000,'����������� ���','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','7','�����',1000,'���������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','8','�����',1000,'������ ������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','9','����',1000,'����� ��������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','10','�����',1000,'����� ������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','11','�����',1000,'����� �����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','12','����',1000,'��� ���','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','13','�����',1000,'����� �������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','14','�����',1000,'���������� ����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','15','�����',1000,'����������� �������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','16','�����',1000,'����� �����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','17','����',1000,'��������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','18','�����',1000,'������������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','19','�����',1000,'������������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','20','����',1000,'����� ���������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','21','����',1000,'��������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','22','�����',1000,'����������� ��������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','23','�����',1000,'������������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','24','���',1000,'���','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','25','����',1000,'����','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','26','����',1000,'������ �����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','27','����',1000,'������ ������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','28','����',1000,'������ ����','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','29','�����',1000,'����������� ���������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','30','�����',1000,'��������� �����','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','31','�����',1000,'����������� �����','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;
    execute procedure SPR_OPV_INSERT_ROOM(:room_kinds_id,'�����','32','�����',1000,'�����������','')returning_values (:step,:STEP_INFO,:msg,:return_id); suspend;

  end
else
  MSG = 'error: PK_id';
suspend;
--================================================================================================

end^
commit^

create or alter procedure SPR_OPV_CREATE_BUILDS
returns (
    STEP varchar(50),
    STEP_INFO varchar(50),
    MSG varchar(50))
as
declare variable BUILD_ID integer;
begin
  step = '�������: ������'; STEP_INFO ='����������� ����'; MSG = 'error';
  BUILD_ID = 0;
  select b.id from building b where b.code = '��' into :BUILD_id;
  if (BUILD_id = 0) then
  begin
    BUILD_id = GEN_ID(G_BUILD,1);
    INSERT INTO BUILDING (ID, CODE, ROOMS, NAME, HOTEL) VALUES (:BUILD_id, '��', 0, '����������� ����', 1);
    MSG = 'ok';
    suspend;
  end
  else
    MSG = 'No. Dublicate';
  suspend;
  --===============================================================
  step = '�������: ������'; STEP_INFO ='���������'; MSG = 'error';
  BUILD_ID = 0;
  select b.id from building b where b.code = '�����' into :BUILD_id;
  if (BUILD_id = 0) then
  begin
    BUILD_id = GEN_ID(G_BUILD,1);
    INSERT INTO BUILDING (ID, CODE, ROOMS, NAME, HOTEL) VALUES (:BUILD_id, '�����', 0, '���������', 1);
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  suspend;
   --===============================================================
  step = '�������: ������'; STEP_INFO ='���������(�)'; MSG = 'error';
  BUILD_ID = 0;
  select b.id from building b where b.code = '������' into :BUILD_id;
  if (BUILD_id = 0) then
  begin
    BUILD_id = GEN_ID(G_BUILD,1);
    INSERT INTO BUILDING (ID, CODE, ROOMS, NAME, HOTEL) VALUES (:BUILD_id, '������', 0, '���������(�)', 1);
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  suspend;
  --===============================================================
  step = '�������: ������'; STEP_INFO ='����������'; MSG = 'error';
  BUILD_ID = 0;
  select b.id from building b where b.code = '�����' into :BUILD_id;
  if (BUILD_id = 0) then
  begin
    BUILD_id = GEN_ID(G_BUILD,1);
    INSERT INTO BUILDING (ID, CODE, ROOMS, NAME, HOTEL) VALUES (:BUILD_id, '�����', 0, '����������', 1);
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  suspend;
  --===============================================================
  step = '�������: ������'; STEP_INFO ='�����'; MSG = 'error';
  BUILD_ID = 0;
  select b.id from building b where b.code = '�����' into :BUILD_id;
  if (BUILD_id = 0) then
  begin
    BUILD_id = GEN_ID(G_BUILD,1);
    INSERT INTO BUILDING (ID, CODE, ROOMS, NAME, HOTEL) VALUES (:BUILD_id, '�����', 0, '�����', 1);
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
    suspend;
  --===============================================================

end
^
commit^
create or alter procedure SPR_OPV_CREATE_1_ROOMS
returns (
    STEP varchar(50),
    STEP_INFO varchar(50),
    MSG varchar(50))
as
begin

  for select spr.step, spr.step_info, spr.msg from SPR_OPV_CREATE_BUILDS spr
  into :step,:step_info, :msg do  begin  suspend;  end

  for select spr.step, spr.step_info, spr.msg from SPR_OPV_CREATE_ROOMS spr
  into :step,:step_info, :msg do begin  suspend;  end

  for select spr.step, spr.step_info, spr.msg from SPR_OPV_CREATE_ROOMS2 spr
  into :step,:step_info, :msg do begin  suspend;  end

  for select spr.step, spr.step_info, spr.msg from SPR_OPV_CREATE_ROOMS3 spr
  into :step,:step_info, :msg do begin  suspend;  end

end^
commit^
create or alter procedure SPR_OPV_CREATE_1_OPV2
returns (
    GK_NAME varchar(1000),
    CONDITIONS varchar(1000))
as
declare variable POID_LIST varchar(1000);
declare variable CONDITIONLIST varchar(1000);
declare variable I integer;
declare variable PO_ID integer;
declare variable GROUP_KIND_ID integer;
declare variable OPV_KIND_ID integer;
begin
  delete from OPV_KIND;
  delete from OPV_OPV;
  Group_Kind_ID = 0;
  CONDITIONS = '';
  CONDITIONLIST = '';
  for select
  case
    when po.name in ('���������','�������','����������� ') then   '������'
    when po.name in ('�������������','���������','������')            then   '����������������'
    when po.name in ('������','����������')             then   '����'
    when po.name in ('�����','������','��������')   then   '������'
    when po.name in ('��������','�������','����������� ') then   '��������'
    when po.name in ('����������','����������','��������','��������','���������') then   '�������'
    when po.name in ('��������','�������','��������')   then   '������'
    when po.name in ('�������','��������','������')    then   '����'

    when po.name in ('��������','�����','���������')    then   '���������'

    when po.name in ('������')                          then   '������'
    when po.name in ('��������')                          then   '��������'
    when po.name in ('���������')                       then   '���������'
    else 'xxx'
  end,
  list(''||cast (po.id as varchar (6))||'','#')
from property_objects po
group by 1
  into :GK_name, :poID_list do
  begin
    poID_list = '#'||:poID_list||'#';
    i = 0;
    --========================================
    OPV_KIND_ID = gen_id(gen_opv_kind_id,1);
    INSERT INTO OPV_KIND (ID, GROUPID, CODE, NAME) VALUES (:OPV_KIND_ID, 1, '$GK'||Cast(:i as varchar (2)), :GK_name);
    for
      select po.id from property_objects po where pos('#'||cast(po.id as varchar (6))||'#',:poID_list)>0
    into :PO_ID do
    begin
      INSERT INTO OPV_OPV (KINDID, PO_ID,enabled) VALUES (:OPV_KIND_ID, :po_id,1);
    end
    --========================================




    CONDITIONS ='';
    CONDITIONLIST = '';
    Group_Kind_ID = Group_Kind_ID+1;
  WHILE(i < 9)DO
  BEGIN
    CONDITIONS = '';
    CONDITIONLIST = '';
    i = i +1;
     for
        select po.id from property_objects po where pos('#'||cast(po.id as varchar (6))||'#',:poID_list)>0
      into :PO_ID do
      begin
        if (CONDITIONS = '') then
          CONDITIONS = '(:priority_prop_obj'||cast(:PO_ID as varchar(10))||'$ >= '||cast(i as varchar (2))||') ';

        else CONDITIONS = CONDITIONS||'AND(:priority_prop_obj'||cast(:PO_ID as varchar(10))||'$ >= '||cast(i as varchar (2))||') ';

        if (CONDITIONLIST = '') then
          CONDITIONLIST = '&ConditionList=:priority_prop_obj'||:PO_ID||'$ ';
        else CONDITIONLIST = CONDITIONLIST||'|:priority_prop_obj'||:PO_ID||'$ ';
      end
      execute procedure SPR_OPV_INSERT_RULES('$GK'||Cast(Group_Kind_ID as varchar (3))||cast(i as varchar (2)),
                                             GK_name,10+i,CONDITIONS,CONDITIONLIST
                                            );


    end

  end
   suspend;
end^
commit^
create or alter procedure SPR_OPV_CREATE_1_OPV
returns (
    ST varchar(10))
as
declare variable I_PO integer;
declare variable NAME varchar(100);
declare variable PDS_VALUE integer;
declare variable NAME_ADD D_LARGESTR;
declare variable NAME_BEFORE varchar(100);
begin
i_po =0; name_before = '';
--===================================================================================
name ='���������';    name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������';       name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='����������� '; name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�������������';name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������ ';      name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������';       name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='���������� ';  name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�����';        name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������';       name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�������� ';    name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�������';      name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='����������� '; name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������� ';   name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='����������';   name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='����������';   name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='���������';    name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�������';      name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�������';      name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������';       name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='��������';     name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='�����';        name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='���������';    name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);
name ='������';       name_add = ''; pds_value = 0;     if (name<>name_before) then  i_po = i_po+1; name_before = name;  EXECUTE PROCEDURE SPR_OPV_INSERT_PR_3(i_po,name,name_add,pds_value);

--===================================================================================
   st = 'OK';
  suspend;
end^
commit^
create or alter procedure SPR_OPV_CREATE_1
returns (
    STEP varchar(50),
    STEP_INFO varchar(50),
    MSG varchar(50))
as
declare variable PO_ID integer;
declare variable I integer;
begin
  --===============================================================
  step = '�������: �������� ������� � ��������'; STEP_INFO ='$OPIT'; MSG = 'error';
  po_id = 0;
  select first(1) po.id from property_objects po where po.code = '$OPIT' and po.enabled = 1 into :po_id;
  if (po_id = 0) then
  begin
    PO_ID = gen_id(g_Property_objects,1);
    insert into property_objects (id,code, name,enabled, paramstr,checkunique,CHECK_RIGHT_TO_MODIFY,USEPRIORITY)
           values (:PO_ID,'$OPIT','���� �����',1,'&tablenames=GUESTS|ROOMS',0,1,1);
    i = 0;
    while (i < 200) do
    begin
      i =i+1;
      INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PO_ID, :i, 1   , :i, '$OP'||:i);
    end
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  suspend;
  --===============================================================
  step = '�������: �������� ������� � ��������'; STEP_INFO ='$D_END'; MSG = 'error';
  po_id = 0;
  select first(1) po.id from property_objects po where po.code = '$D_END' and po.enabled = 1 into :po_id;
  if (po_id = 0) then
  begin
    PO_ID = gen_id(g_Property_objects,1);
    insert into property_objects (id,code, name,enabled, paramstr,checkunique)
           values (:PO_ID,'$D_END','��������?',1,'&tablenames=DIARY_KIND2',0);
    INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PO_ID, '��', 1   , 1, '$D_END1');
    INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PO_ID, '���', 1   , 2, '$D_END0');
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  --===============================================================
  suspend;
  --===============================================================
  step = '�������: �������� ������� � ��������'; STEP_INFO ='$ISCEN'; MSG = 'error';
  po_id = 0;
  select first(1) po.id from property_objects po where po.code = '$ISCEN' and po.enabled = 1 into :po_id;
  if (po_id = 0) then
  begin
    PO_ID = gen_id(g_Property_objects,1);
    insert into property_objects (id,code, name,enabled, paramstr,checkunique,CHECK_RIGHT_TO_MODIFY,USEPRIORITY)
           values (:PO_ID,'$ISCEN','��� ������ �����',1,'&tablenames=ROOMS',0,1,1);
    INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PO_ID, '���', 1   , 1, '$ISCEN1');
    INSERT INTO RESOLVED_VALUES ( PROPERTY_OBJECT, STRVALUE, ENABLED, PRIORITY, CODE) VALUES (:PO_ID, '��', 1   , 2, '$ISCEN2');
    MSG = 'ok';
  end
  else
    MSG = 'No. Dublicate';
  --===============================================================
  suspend;
end^
commit^
create or alter procedure SPR_OPV_CREATE_0_ROOMS
returns (
    MSG D_LARGESTR)
as
begin
  delete from rooms r;
  delete from room_kinds rk;
  delete from resource_categ rk;
  delete from building b;
  delete from obj_prop_values opv where xupper(opv.tablename) = xupper('rooms');
  msg = 'ok';
  suspend;
end^
commit^
create or alter procedure SPR_OPV_CREATE_0_OPV
returns (
    ST varchar(10))
as
begin
  --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  delete from obj_prop_values opv where xupper(opv.tablename) in ( xupper('guests'),xupper('Diary_kinds'));-- where (po.code like '$PR%') or (po.code like '$PDSB') or (po.code is null) ;
  delete from property_objects po where (po.code like '$PR%') or (po.code like '$PDSB')or (po.code ='$OPIT')or (po.code ='$D_END') or (po.code ='$ISCEN') or (po.code is null) ;
  delete from RESOLVED_VALUES rv  where (rv.code like '$PR%')or (rv.code like '$PDSB%')or (rv.code like '$D_END%')or (rv.code like '$OP%')or (rv.code like '$ISCEN%') or (rv.code is null) ;
  delete from diary_kinds dk where (dk.code like '$TS%') or (DK.code is null) ;
  delete from RULES_FOR_CONDITIONS rfc ;--where (rfc.code like '$RC%') or (rfc.code is null) ;
--  delete from OPV_KIND;
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- $PDSBN% insert =====================================================
 --  EXECUTE PROCEDURE spr_opv_insert_pds;
 --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  st = 'ok'; 
  suspend;
end^
commit^
create or alter procedure SPR_OPV_CREATE_0
as
begin
  /* Procedure Text */
  suspend;
end^
commit^

create or alter procedure SPR_OPV_ACTION_GAME (
    DIARY_ID integer)
returns (
    SUCCESS integer,
    ERROR integer,
    PO_NAME D_LARGESTR,
    OPV_STRVALUE D_LARGESTR)
as
declare variable OPV_SUCCESS integer;
declare variable PDS_SUCCESS integer;
declare variable OPV_OPIT_SUCCESS integer;
declare variable ERROR_PARAM D_STR100;
declare variable GUEST_ID integer;
declare variable DIARY_PO_ID integer;
declare variable PO_CODE D_CODE;
declare variable DIARY_COUNT integer;
declare variable STEPM_PRIORITY integer;
declare variable OPIT integer;
declare variable PDS_COST integer;
begin
 for
   select
     d.guest,
     po.id,
     po.code,
     po.name
   from diary d
     inner join diary dg on dg.id = d.inquiry  and dg.enabled = 1
     inner join diary_kinds dk on dk.id = dg.diary_kind and dk.enabled = 1 and (dk.code like '$TS___')
     inner join obj_prop_values opv on opv.ref = dk.id and  opv.enabled = 1 and  xupper(opv.tablename) = xupper('Diary_kinds')
     inner  join property_objects po on po.id = opv.property_object and (po.code like '$PR__') and po.enabled = 1
   where 1=1
     and d.kind in (2)
     and d.enabled = 1
     and dg.status = 2
     and d.id = :diary_id
 into :guest_id,:diary_po_id,:po_code,:po_name do
 begin
   --===============================================
   select
     sum(1) diary_count
   from diary d
     inner join diary dg on dg.id = d.inquiry  and dg.enabled = 1
     inner join diary_kinds dk on dk.id = dg.diary_kind and dk.enabled = 1
     inner join obj_prop_values opv on opv.ref = dk.id and  opv.enabled = 1 and  upper(opv.tablename) = upper('Diary_kinds')
     inner join property_objects po on po.id = opv.property_object and po.enabled = 1 and po.id = :diary_po_id
     left join resolved_values rv on rv.id = opv.resolved_value and  rv.enabled = 1
   where 1=1
     and d.kind in (2)
     and d.enabled = 1
     and dg.status = 2
     and d.guest = :guest_id
   into :diary_count;
   stepm_priority = :diary_count;
   -----------------------------------------------------------------------------
   select rv.strvalue from  property_objects po
     left join resolved_values rv on rv.property_object = po.id and  rv.enabled = 1
   where po.id = :diary_po_id
     and rv.priority = :stepm_priority
   into :opv_strvalue ;
   -----------------------------------------------------------------------------
   select cast(snn(spr.strvalue)as integer) from spr_getobjpropvalue ('GUESTS',:GUEST_ID,'$OPIT','')spr into :opit;
   opit = opit +1;
   --===============================================

--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  opv_success = -1;
  pds_success = -1;
  success = 0;
  error = 0;

--==============================================
  select success from  SPR_SETOBJPROPVALUE('guests', :GUEST_ID,:po_code, :opv_strvalue,'', '')
  into :opv_success;
  if (:opv_success = 0) then
    begin
    error = 301001;
    suspend;
    execute procedure SYS$EXCEPTION( :error, '������ ��������� �������� �������. �������: �����.', '' );

    end
--==============================================
select success from  SPR_SETOBJPROPVALUE('guests', :GUEST_ID,'$OPIT', :opit,'', '')
  into :OPV_OPIT_SUCCESS;
  if (:OPV_OPIT_SUCCESS = 0) then
    begin
    error = 301001;  error_param = ' ��������: ���� ';
    execute procedure SYS$EXCEPTION( :error, '������ ��������� �������� �������. �������: �����.'||error_param, '' );
    end
--==============================================
  if (pds_cost <>0) then
    begin
      select iif(spr.error=0,1,0) from  SP_CREATE_PDS_TRANSACTION(0,2,:pds_cost,0,0,0,0,0,0,0,0,0,0,0,0,:guest_id,0,'') spr
      into :pds_success;
    end


  if ((opv_success = 1)and (OPV_OPIT_SUCCESS=1)) then
    if ((PDS_COST> 0) and (pds_success = 0)) then
      success = 0;
    else  success =1 ;
  else
    begin
    success =0 ;
    end
   suspend;
  end
end^
commit^
SET TERM ; ^
