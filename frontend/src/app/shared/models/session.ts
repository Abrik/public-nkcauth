export interface ISession {
    id: number;
    companyName: string;
    staffFullname: string;
    sessionStart: Date;
    sessionEnd: Date;
    sessionDuration: string;
  }