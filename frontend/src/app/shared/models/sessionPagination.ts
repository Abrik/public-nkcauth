import { ISession } from "./session";

export interface ISessionPagination {
    pageIndex: number;
    pageSize: number;
    count: number;
    data: ISession[];
  }
  