export interface IAuth {
    companyId: number;
    email: string;
    isEnter: boolean;
  }