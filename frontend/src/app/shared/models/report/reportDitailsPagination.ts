import { IReportDitail } from "./reportDitail";

export interface IReportFullPagination {
    pageIndex: number;
    pageSize: number;
    count: number;
    data: IReportDitail[];
  }
  