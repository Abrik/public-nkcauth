import { IReportGroup } from "./reportGroup";

export interface IReportGroupPagination {
    pageIndex: number;
    pageSize: number;
    count: number;
    data: IReportGroup[];
  }
  