export interface IReportGroup {
    groupDate: string;
    companyName: string;
    staffFullname: string;
    sessionDuration: number;
  }