import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { SessionComponent } from './session/session.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'not-found', component: NotFoundComponent},
  { path: 'auth', component: AuthComponent},  
  { path: 'session', component: SessionComponent},  
  { path: 'account', loadChildren:()=>import('./account/account.module').then(mod => mod.AccountModule)},  
  { path: 'report', loadChildren:()=>import('./report/report.module').then(mod => mod.ReportModule)},  
  { path: '**', redirectTo:'not-found', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
