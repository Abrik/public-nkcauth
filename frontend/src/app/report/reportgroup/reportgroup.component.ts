import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IReportDitail } from 'src/app/shared/models/report/reportDitail';
import { IReportGroup } from 'src/app/shared/models/report/reportGroup';
import { SessionParams } from 'src/app/shared/models/sessionParams';
import { ReportService } from '../report.service';

@Component({
  selector: 'app-reportgroup',
  templateUrl: './reportgroup.component.html',
  styleUrls: ['./reportgroup.component.scss']
})
export class ReportgroupComponent implements OnInit {
  sessionParams = new SessionParams();
  totalCount: number;
  data: IReportGroup[];

  constructor(
    private reportService:ReportService,
    private toastr: ToastrService) { }

  ngOnInit(): void {   
    this.getValues();
  }
  getValues(){
    this.reportService.getReportgroup(this.sessionParams).subscribe(response =>{
      this.data = response.data;
      this.sessionParams.pageNumber = response.pageIndex;
      this.totalCount = response.count;
    }, error =>{
      this.toastr.error(error);
    });
  }

  onPageChanged(event: any){
    this.sessionParams.pageNumber= event.page;
    this.getValues();
  }

}
