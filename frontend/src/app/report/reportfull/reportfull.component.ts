import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ICompany } from 'src/app/shared/models/company';
import { IReportDitail } from 'src/app/shared/models/report/reportDitail';
import { SessionParams } from 'src/app/shared/models/sessionParams';
import { ReportService } from '../report.service';

@Component({
  selector: 'app-reportfull',
  templateUrl: './reportfull.component.html',
  styleUrls: ['./reportfull.component.scss']
})
export class ReportfullComponent implements OnInit {
  sessionParams = new SessionParams();
  totalCount: number;
  data: IReportDitail[];
  
  constructor(
    private reportService:ReportService,
    private toastr: ToastrService) { }

  ngOnInit(): void {   
    this.getValues();
  }
  getValues(){
    this.reportService.getReportfull(this.sessionParams).subscribe(response =>{
      this.data = response.data;
      this.sessionParams.pageNumber = response.pageIndex;
      this.totalCount = response.count;
    }, error =>{
      this.toastr.error(error);
    });
  }

  onPageChanged(event: any){
    this.sessionParams.pageNumber= event.page;
    this.getValues();
  }

}
