import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report.component';
import { ReportfullComponent } from './reportfull/reportfull.component';
import { ReportgroupComponent } from './reportgroup/reportgroup.component';
import { ReportRoutingModule } from './report-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    ReportComponent,
    ReportfullComponent,
    ReportgroupComponent
  ],
  imports: [
    CommonModule,
    ReportRoutingModule,
    SharedModule
  ],
  exports:[
    ReportComponent
  ]
})

export class ReportModule { }
