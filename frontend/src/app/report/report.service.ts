import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IReportDitail } from '../shared/models/report/reportDitail';
import { IReportFullPagination } from '../shared/models/report/reportDitailsPagination';
import { IReportGroupPagination } from '../shared/models/report/reportGroupPagination';
import { ISessionPagination } from '../shared/models/sessionPagination';
import { SessionParams } from '../shared/models/sessionParams';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  baseUrl = environment.apiUrl;  
  constructor(private http: HttpClient){}

  getReportfull(sessionParams: SessionParams){
    let params = new HttpParams();
    params = params.append("pageIndex", sessionParams.pageNumber.toString());
    params = params.append("pageSize", sessionParams.pageSize.toString());

    return this.http.get<IReportFullPagination>(this.baseUrl+'report/reportfull',{observe: 'response', params})
    .pipe(
      map(response => {
        return response.body;
      })
    );

  }

  getReportgroup(sessionParams: SessionParams){
    let params = new HttpParams();
    params = params.append("pageIndex", sessionParams.pageNumber.toString());
    params = params.append("pageSize", sessionParams.pageSize.toString());

    return this.http.get<IReportGroupPagination>(this.baseUrl+'report/reportgroup',{observe: 'response', params})
    .pipe(
      map(response => {
        return response.body;
      })
    );

  }

  getAll(sessionParams: SessionParams){
    let params = new HttpParams();

    if(sessionParams.accountId > 0)
      params = params.append("accountId", sessionParams.accountId.toString());
    
    if(sessionParams.companyId > 0)
      params = params.append("companyId", sessionParams.companyId.toString());
    
    if(sessionParams.sort)
      params = params.append("sort", sessionParams.sort);

    params = params.append("isVisibleFinish", sessionParams.isVisibleFinish.toString());  
    params = params.append("isVisibleNotFinish", sessionParams.isVisibleNotFinish.toString()); 

    params = params.append("pageIndex", sessionParams.pageNumber.toString());
    params = params.append("pageSize", sessionParams.pageSize.toString());

    return this.http.get<ISessionPagination>(this.baseUrl+'session',{observe: 'response', params})
    .pipe(
      map(response => {
        return response.body;
      })
    );
  }
}