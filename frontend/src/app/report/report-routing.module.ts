import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportfullComponent } from './reportfull/reportfull.component';
import { ReportgroupComponent } from './reportgroup/reportgroup.component';
import { ReportComponent } from './report.component';


const routes: Routes =[
  {path: '', component: ReportComponent},
  {path: 'reportfull', component: ReportfullComponent},
  {path: 'reportgroup', component: ReportgroupComponent}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
