import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IAccount } from '../shared/models/account';
import { ICompany } from '../shared/models/company';
import { IStaff } from '../shared/models/staff';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl = environment.apiUrl;  
  constructor(private http: HttpClient){}

  login(values: any){
    return this.http.post(this.baseUrl+'account/login',values);
  }

  logout(values: any){
    return this.http.post(this.baseUrl+'account/logout',values);
  }
  getAll(){
    return this.http.get<IAccount[]>(this.baseUrl+'account');
  }
}