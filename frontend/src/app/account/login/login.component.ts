import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CompanyService } from 'src/app/company/company.service';
import { ICompany } from 'src/app/shared/models/company';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginFrom: FormGroup;

  companies: ICompany[];
  staffEmail : string;
  companyId: number =0;

  constructor(private accountService: AccountService, 
    private companyService: CompanyService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.createLoginForm();

    this.companyService.getAll().subscribe(response =>{
      this.companies = response;
    }, error =>{
      this.toastr.error(error);
    });    
  }

  createLoginForm(){
    this.loginFrom = new FormGroup({      
      companyId: new FormControl('',Validators.required), 
      email: new FormControl('',Validators.required)
    })
  }

  onSubmit(){
    this.accountService.login(this.loginFrom.value).subscribe(()=>{
      this.toastr.success("Успешный вход");
      this.createLoginForm();
    }, error => {
      if(error.errors)
         this.toastr.error(error.errors);
    });
  }


}
