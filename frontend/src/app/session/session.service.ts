import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ISession } from '../shared/models/session';
import { ISessionPagination } from '../shared/models/sessionPagination';
import { SessionParams } from '../shared/models/sessionParams';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  baseUrl = environment.apiUrl;    
  constructor(private http: HttpClient){}

  getAll(sessionParams: SessionParams){
    let params = new HttpParams();

    if(sessionParams.accountId > 0)
      params = params.append("AccountId", sessionParams.accountId.toString());
    
    if(sessionParams.companyId > 0)
      params = params.append("CompanyId", sessionParams.companyId.toString());
    
    if(sessionParams.sort)
      params = params.append("Sort", sessionParams.sort);

    params = params.append("IsVisibleFinish", sessionParams.isVisibleFinish.toString());  
    params = params.append("IsVisibleNotFinish", sessionParams.isVisibleNotFinish.toString()); 

    params = params.append("PageIndex", sessionParams.pageNumber.toString());
    params = params.append("PageSize", sessionParams.pageSize.toString());

    return this.http.get<ISessionPagination>(this.baseUrl+'session',{observe: 'response', params})
    .pipe(
      map(response => {
        return response.body;
      })
    );
  }
}

  