import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account/account.service';
import { CompanyService } from '../company/company.service';
import { ReportService } from '../report/report.service';
import { IAccount } from '../shared/models/account';
import { ICompany } from '../shared/models/company';
import { ISession } from '../shared/models/session';
import { SessionParams } from '../shared/models/sessionParams';
import { SessionService } from './session.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  sessions: ISession[];
  companies: ICompany[];
  accounts: IAccount[];
  sessionParams = new SessionParams();
  totalCount: number;
  constructor(private sessionService: SessionService, 
    private companyService: CompanyService, 
    private accountService: AccountService,
    private reportService:ReportService) { }

  ngOnInit(): void {
    this.getValues();  

    this.companyService.getAll().subscribe(response =>{
      this.companies = response;
    }, error =>{
      console.log(error);
    });

    this.accountService.getAll().subscribe(response =>{
      this.accounts = response;      
      console.log(response);
    }, error =>{
      console.log(error);
    });
  }

  getValues(){
    console.log("getValues");
    console.log(this.sessionParams);
    this.sessionService.getAll(this.sessionParams).subscribe(response =>{
      this.sessions = response.data;
      this.sessionParams.pageNumber = response.pageIndex;
      this.totalCount = response.count;
      console.log(response.data);
    }, error =>{
      console.log(error);
    });
  }

  sortValues(field: string): void {
    if (this.sessionParams.sort!=field+"Asc")
      this.sessionParams.sort=field+"Asc"
    else  
      this.sessionParams.sort=field+"Desc";

    this.getValues(); 
  }

  onPageChanged(event: any){
    this.sessionParams.pageNumber= event.page;
    this.getValues();
  }
}
