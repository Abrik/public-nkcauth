import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    CommonModule,
    FormsModule ,
    ToastrModule.forRoot({
      positionClass:'toast-bottom-right',
      preventDuplicates: true
    })   
  ],
  exports:[
    AuthComponent
  ]
})
export class AuthModule { }
