import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { IAuth } from '../shared/models/auth';
import { ICompany } from '../shared/models/company';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  companies: ICompany[];
  staffEmail : string;
  companyId: number =0;

  constructor(private authService: AuthService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.authService.getCompanies().subscribe(response =>{
      this.companies = response;
      console.log(response);
    }, error =>{
      console.log(error);
    });    
  }

  postLogin(){    
    console.log("postLogin:"+this.staffEmail+" "+this.companyId)
    const body = {companyId: this.companyId, email: this.staffEmail,isEnter: true};
    this.authService.postAction(body).subscribe(response =>{      
      console.log("response" + response.toString());
      this.toastr.success(response.toString());
    }, error => {
      console.log("error" + error.error.toString());
      this.toastr.error(error.toString());
    })
  }

  postLogout(){    
    console.log("postLogout:"+this.staffEmail+" "+this.companyId)
    const body = {companyId: this.companyId, email: this.staffEmail, isEnter: false};
    this.authService.postAction(body).subscribe(response =>{
      this.toastr.success(response.toString());
    }, error => {
      this.toastr.error(error.toString());
    })
  }

}
