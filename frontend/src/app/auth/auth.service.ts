import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IAuth } from '../shared/models/auth';
import { ICompany } from '../shared/models/company';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl;  
  constructor(private http: HttpClient){}

  getCompanies(){
    console.log(this.baseUrl);
    return this.http.get<ICompany[]>(this.baseUrl+'company');
  }

  postAction(auth: any){
    console.log(this.baseUrl);
    return this.http.post(this.baseUrl+'auth/action',auth);
  }
}