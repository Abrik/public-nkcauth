import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ICompany } from '../shared/models/company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  baseUrl = environment.apiUrl;  
  constructor(private http: HttpClient){}

  getAll(){
    return this.http.get<ICompany[]>(this.baseUrl+'company');
  }
}