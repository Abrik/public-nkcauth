﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Config
{
    class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(c => c.IsDeleted).HasDefaultValue(false);
            builder.Property(c => c.Name).IsRequired().HasMaxLength(30);
            builder.Property(c => c.Address).HasMaxLength(100);
            builder.HasMany(p => p.Staffs)
                    .WithMany(c => c.Companies);

        }
    }
}
