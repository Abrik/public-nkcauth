﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Config
{
    class StaffConfiguration : IEntityTypeConfiguration<Staff>
    {
        public void Configure(EntityTypeBuilder<Staff> builder)
        {
            builder.HasKey(x=> x.Id);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
            builder.Property(x => x.Name).HasMaxLength(30);
            builder.Property(x => x.SurName).HasMaxLength(30);
            builder.Property(x => x.MiddleName).HasMaxLength(30);            
            builder.Property(x => x.Email).HasMaxLength(30);
            builder.HasIndex(x => x.Email).IsUnique();
            builder.HasMany(p => p.Companies)
                    .WithMany(c => c.Staffs);
        }
    }
}
