﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Core.Specifications;
using System.Linq.Expressions;

namespace Infrastructure.Data
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AuthContext _context;

        public GenericRepository(AuthContext context)
        {
            _context = context;
        }

        public async  Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<T> GetPredicateFirstAsync(Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IReadOnlyList<T>> GetPredicateListAsync(Func<T, bool> predicate)
        {
            return await _context.Set<T>().Where(predicate).AsQueryable().ToListAsync();
        }
        public async Task<IReadOnlyList<T>> GetWhithIncluseAsync(Func<T, bool> predicate, params Expression<Func<T, object>>[] inqludeProperties)
        {
            return await Include(inqludeProperties).ToListAsync();
        }
        
        

        public async Task<IReadOnlyList<T>> GetListAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetWithSpec(ISpecification<T> spec)
        {
            return await ApplaySpecification(spec).FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyList<T>> GetListWithSpec(ISpecification<T> spec)
        {
            return await ApplaySpecification(spec).ToListAsync();
        }

        public async Task<int> CountWithSpecAsync(ISpecification<T> spec)
        {
            return await ApplaySpecification(spec).CountAsync();
        }

        private IQueryable<T> ApplaySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator<T>.GetQuery(_context.Set<T>().AsQueryable(), spec);
        }

        public void  Update(T item)
        {
            _context.Set<T>().Update(item);         
            _context.SaveChanges();
        }
        public async Task AddAsync(T item)
        {
            await _context.Set<T>().AddAsync(item);
            await _context.SaveChangesAsync();
        }

        private IQueryable<T> Include(params Expression<Func<T, object>>[] inqludeProperties)
        {
            IQueryable<T> query = _context.Set<T>();
            return inqludeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

        }
    }
}
