﻿using Core.Entities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class AuthContextSeed
    {
        public static async Task SeedAsync(AuthContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!context.Companies.Any())
                {
#if (DEBUG)                   
                    var Data = File.ReadAllText("../Infrastructure/Data/SeedData/companies.json");
#else
                    var Data = File.ReadAllText(Path.GetFullPath("Data/SeedData/companies.json"));
#endif

                    var companies = JsonConvert.DeserializeObject<List<Company>>(Data);
                    foreach (var item in companies)
                    {
                        context.Companies.Add(item);
                    }
                    await context.SaveChangesAsync();
                }

                if (!context.Staffs.Any())
                {
#if DEBUG
                    var Data = File.ReadAllText("../Infrastructure/Data/SeedData/staffs.json");
#else
                    var Data = File.ReadAllText(Path.GetFullPath("Data/SeedData/staffs.json"));
#endif
                    var items = JsonConvert.DeserializeObject<List<Staff>>(Data);
                    foreach (var item in items)
                    {
                        List<Company> companys = null; 
                        if (item.Id == 1)
                            companys = context.Companies.Where(x => x.Id == 1 || x.Id == 3).ToList();
                        else
                            companys = context.Companies.Where(x => x.Id == 2 || x.Id == 4 || x.Id == 5).ToList();

                        companys.ForEach(x => item.Companies.Add(x));

                        context.Staffs.Add(item);
                    }
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<AuthContextSeed>();
                logger.LogError(ex.Message);
            }
        }
    }
}
