﻿// <auto-generated />
using System;
using FirebirdSql.EntityFrameworkCore.Firebird.Metadata;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.Migrations
{
    [DbContext(typeof(AuthContext))]
    [Migration("20210428180402_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 31)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("Fb:ValueGenerationStrategy", FbValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CompanyStaff", b =>
                {
                    b.Property<int>("CompaniesId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("StaffsId")
                        .HasColumnType("INTEGER");

                    b.HasKey("CompaniesId", "StaffsId");

                    b.HasIndex("StaffsId");

                    b.ToTable("CompanyStaff");
                });

            modelBuilder.Entity("Core.Entities.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Address")
                        .HasMaxLength(100)
                        .HasColumnType("VARCHAR(100)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("BOOLEAN")
                        .HasDefaultValue(false);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(30)
                        .HasColumnType("VARCHAR(30)");

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("Core.Entities.Report", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("TIMESTAMP");

                    b.Property<byte[]>("Data")
                        .IsRequired()
                        .HasColumnType("BLOB SUB_TYPE BINARY");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("BOOLEAN")
                        .HasDefaultValue(false);

                    b.HasKey("Id");

                    b.ToTable("Reports");
                });

            modelBuilder.Entity("Core.Entities.Session", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("CompanyId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("TIMESTAMP");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("BOOLEAN")
                        .HasDefaultValue(false);

                    b.Property<int>("StaffId")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("TIMESTAMP");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("StaffId");

                    b.ToTable("Sessions");
                });

            modelBuilder.Entity("Core.Entities.Staff", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Email")
                        .HasMaxLength(30)
                        .HasColumnType("VARCHAR(30)");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("BOOLEAN")
                        .HasDefaultValue(false);

                    b.Property<string>("MiddleName")
                        .HasMaxLength(30)
                        .HasColumnType("VARCHAR(30)");

                    b.Property<string>("Name")
                        .HasMaxLength(30)
                        .HasColumnType("VARCHAR(30)");

                    b.Property<string>("SurName")
                        .HasMaxLength(30)
                        .HasColumnType("VARCHAR(30)");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("Staffs");
                });

            modelBuilder.Entity("CompanyStaff", b =>
                {
                    b.HasOne("Core.Entities.Company", null)
                        .WithMany()
                        .HasForeignKey("CompaniesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Staff", null)
                        .WithMany()
                        .HasForeignKey("StaffsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Core.Entities.Session", b =>
                {
                    b.HasOne("Core.Entities.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Core.Entities.Staff", "Staff")
                        .WithMany()
                        .HasForeignKey("StaffId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Company");

                    b.Navigation("Staff");
                });
#pragma warning restore 612, 618
        }
    }
}
