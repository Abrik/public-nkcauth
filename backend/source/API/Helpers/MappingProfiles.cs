﻿using AutoMapper;
using Core.DTO.Account;
using Core.DTO.Report;
using Core.DTO.Session;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Helpers
{
    public class MappingProfiles: Profile
    {
        public MappingProfiles()
        {
            CreateMap<Session, SessionDto>()
            .ForMember(d => d.CompanyName, o => o.MapFrom(s => s.Company.Name))
            .ForMember(d => d.StaffFullname, o => o.MapFrom(s => s.Staff.Fullname))
            .ForMember(d => d.SessionStart, o => o.MapFrom(s => s.StartDate))
            .ForMember(d => d.SessionEnd, o => o.MapFrom(s => s.EndDateLogic))
            .ForMember(d => d.SessionDuration, o => o.MapFrom(s => s.DurationFromat));

            CreateMap<Staff, AccountDto>()
            .ForMember(d => d.Id, o => o.MapFrom(s => s.Id))
            .ForMember(d => d.Fullname, o => o.MapFrom(s => s.Fullname));
        }
    }
}
