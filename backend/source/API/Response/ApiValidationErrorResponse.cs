﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Response
{
    public class ApiValidationErrorResponse : ApiResponse
    {
        public ApiValidationErrorResponse() : base(400)
        {
        }

        public ApiValidationErrorResponse(string Message) : base(400)
        {
            Errors = new[] { Message };
        }

        public IEnumerable<string> Errors { get; set; }
    }
}
