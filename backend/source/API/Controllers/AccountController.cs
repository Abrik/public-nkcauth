﻿using API.Response;
using AutoMapper;
using Core.DTO.Account;
using Core.Entities;
using Core.Interfaces;
using Logic.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            _accountService = accountService;
            _mapper = mapper;
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] AccountActionDto model)
        {
            var msg = await _accountService.Login(model);

            if (!string.IsNullOrWhiteSpace(msg))
                return new BadRequestObjectResult(new ApiValidationErrorResponse(msg));

            return Ok();
        }

        [HttpPost("logout")]
        public async Task<ActionResult> Logout([FromBody] AccountActionDto model)
        {
            var msg = await _accountService.Logout(model);

            if (!string.IsNullOrWhiteSpace(msg))
                return new BadRequestObjectResult(new ApiValidationErrorResponse(msg));

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var staffs = await _accountService.GetAllGeneral();
            return Ok(_mapper.Map<IReadOnlyList<Staff>, IReadOnlyList<AccountDto>>(staffs));
        }
    }
}
