﻿using API.Helpers;
using API.Response;
using AutoMapper;
using Core.DTO.Report;
using Core.DTO.Session;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using FirebirdSql.Data.FirebirdClient;
using Infrastructure.Data;
using Logic.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class SessionController : BaseApiController
    {
        private readonly IGenericRepository<Session> _sessionRepo;
        private readonly IMapper _mapper;

        public SessionController(IGenericRepository<Session> sessionRepo, IMapper mapper)
        {
            _sessionRepo = sessionRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<SessionDto>>> GetAll([FromQuery] SessionSpecParams sessionParams)
        {
            string errorText = "";
            try
            {
                var totalSpec = new SessionCountSpecification(sessionParams);
                var totalCount = await _sessionRepo.CountWithSpecAsync(totalSpec);

                var sesionSpec = new SessionSpecification(sessionParams);
                var sessions = await _sessionRepo.GetListWithSpec(sesionSpec);

                var data = _mapper.Map<IReadOnlyList<Session>, IReadOnlyList<SessionDto>>(sessions);
                return Ok(new Pagination<SessionDto>(
                            sessionParams.PageIndex,
                            sessionParams.PageSize,
                            totalCount,
                            data
                         ));
            }
            catch (Exception ex)
            {
                errorText = ex.Message;
            }

            return Problem(errorText);
        }

    }
}

