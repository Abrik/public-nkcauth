﻿using API.Response;
using Core.Entities;
using Core.Interfaces;
using FirebirdSql.Data.FirebirdClient;
using Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class CompanyController : BaseApiController
    {        
        private readonly IGenericRepository<Company> _repoCompany;

        public CompanyController(IGenericRepository<Company>  repoCompany)
        {
            _repoCompany = repoCompany;
        }

        
        [HttpGet]
         public async Task<ActionResult<List<Company>>> GetCompanies()
        {
            var companies = await _repoCompany.GetListAsync();
            return Ok(companies);
        }
    }
}
