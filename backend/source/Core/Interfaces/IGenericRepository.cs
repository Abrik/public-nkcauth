﻿using Core.Entities;
using Core.Specifications;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IGenericRepository<T> where T: BaseEntity
    {
        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> GetWhithIncluseAsync(Func<T, bool> predicate, params Expression<Func<T, object>>[] inqludeProperties);

        Task<T> GetPredicateFirstAsync(Expression<Func<T, bool>> predicate);
        Task<IReadOnlyList<T>> GetPredicateListAsync(Func<T, bool> predicate);
        Task AddAsync(T item);
        void Update(T item);
        Task<IReadOnlyList<T>> GetListAsync();
        Task<T> GetWithSpec(ISpecification<T> spec);
        Task<IReadOnlyList<T>> GetListWithSpec(ISpecification<T> spec);
        Task<int> CountWithSpecAsync(ISpecification<T> spec);
    }
}
