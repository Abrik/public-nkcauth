﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Report: BaseEntity
    {
        public DateTime CreationDate { get; set; }
        public byte[] Data { get; set; }
    }
}
