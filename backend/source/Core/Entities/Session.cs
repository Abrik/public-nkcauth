﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Session : BaseEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StaffId { get; set; }
        public Staff Staff { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public bool isEnd { get { return EndDate != default(DateTime); } }
        public TimeSpan Duration => (isEnd ? EndDate : DateTime.Now) - StartDate;
        public DateTime EndDateLogic { get { return isEnd ? EndDate : DateTime.Now; } }
        public string StartDateFromat => StartDate.ToString("dd.MM.yyyy HH:mm:ss");
        public string EndDateFromat => (isEnd ? EndDate : DateTime.Now).ToString("dd.MM.yyyy HH:mm:ss");
        public string DurationFromat =>Duration.ToString(@"dd")+ " д. "+ Duration.ToString(@"hh\:mm\:ss");

        
    }
}
