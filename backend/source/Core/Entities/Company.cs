﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Core.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }
        public string Address{ get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }

        public Company()
        {
            Staffs = new List<Staff>();
        }
    }
}
