﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Core.Entities
{
    public class Staff: BaseEntity
    {   
        public string Name { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public string Fullname => $"{SurName} {Name} {MiddleName}"; 

        public Staff()
        {
            Companies = new List<Company>();
        }
    }
}
