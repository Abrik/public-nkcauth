﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Const
{
    public static class ErrorResponceMessage
    {
        public static string EmptyDto = "ПyУстая входящая модель";
        public static string StaffEmailNotFound = "Не найден сотрудник с таким email";
        public const string SessionNotEmpty = "Имеется активная сессия";
        public const string SessionEmpty = "Отсутствует активная сессия";
        public const string StaffNotInCompany = "Сотрудник не работает в этой компании";

        public static string OtherError = "Неизвестная ошибка сервера";
        public const string EmailNotValid = "Не валидный email ";
    }
}
