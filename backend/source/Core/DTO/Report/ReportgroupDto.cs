﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO.Report
{
    public class ReportgroupDto
    {
        public string CompanyName { get; set; }
        public string StaffFullname { get; set; }
        public TimeSpan SessionDuration { get; set; }
    }
}
