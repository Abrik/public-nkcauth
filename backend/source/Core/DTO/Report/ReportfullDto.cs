﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO.Report
{
    public class ReportfullDto
    {
        public int SessionId { get; set; }
        public string CompanyName { get; set; }
        public string StaffFullname { get; set; }
        public DateTime SessionStart { get; set; }
        public DateTime SessionEnd { get; set; }
        public TimeSpan SessionDuration { get; set; }

    }
}
