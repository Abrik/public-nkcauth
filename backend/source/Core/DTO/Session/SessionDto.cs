﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO.Session
{
    public class SessionDto
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string StaffFullname { get; set; }
        public DateTime SessionStart { get; set; }
        public DateTime SessionEnd { get; set; }
        public string SessionDuration { get; set; }

    }
}
