﻿using Core.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.DTO.Account
{
    public class AccountActionDto
    {
        [Required]
        public int CompanyId { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = ErrorResponceMessage.EmailNotValid)]
        public string Email { get; set; }
    }
}
