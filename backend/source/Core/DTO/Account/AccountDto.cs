﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DTO.Account
{
    public class AccountDto
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
    }
}
