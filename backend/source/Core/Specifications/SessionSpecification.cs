﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.Specifications
{
    public class SessionSpecification : BaseSpecification<Session>
    {
        public SessionSpecification(SessionSpecParams par)
            : base(x=>
                (!par.CompanyId.HasValue || x.Company.Id == par.CompanyId)&&
                (!par.AccountId.HasValue || x.Staff.Id == par.AccountId)
          && (
                (x.EndDate == default(DateTime) && par.IsVisibleNotFinish == true) ||
                (x.EndDate != default(DateTime) && par.IsVisibleFinish == true)
             )
           )
        {
            AddInclude(x => x.Staff);
            AddInclude(x => x.Company);
            ApplyPaging(par.PageSize * (par.PageIndex - 1), par.PageSize);

            if (string.IsNullOrWhiteSpace(par.Sort))
                AddOrderBy(x => x.Id);
            else
            {
                switch (par.Sort)
                {
                    case "IdAsc": AddOrderBy(x => x.Id); break;
                    case "IdDesc": AddOrderByDesc(x => x.Id); break;
                    case "CompanyNameAsc": AddOrderBy(x => x.Company.Name); break;
                    case "CompanyNameDesc": AddOrderByDesc(x => x.Company.Name); break;
                    case "SessionStartAsc": AddOrderBy(x => x.StartDate); break;
                    case "SessionStartDesc": AddOrderByDesc(x => x.StartDate); break;
                    case "SessionEndAsc": AddOrderBy(x => x.EndDate); break;
                    case "SessionEndDesc": AddOrderByDesc(x => x.EndDate); break;
                    case "SessionDurationAsc": AddOrderBy(x => x.Duration); break;
                    case "SessionDurationDesc": AddOrderByDesc(x => x.Duration); break;

                    default:
                        AddOrderBy(x => x.Id);
                        break;
                }
            }

        }
        public SessionSpecification(Expression<Func<Session, bool>> criteria) : base(criteria)
        {
        }
    }
}
