﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class SessionCountSpecification: BaseSpecification<Session>
    {
        public SessionCountSpecification(SessionSpecParams par)
            : base(x =>
                (!par.CompanyId.HasValue || x.Company.Id == par.CompanyId) &&
                (!par.AccountId.HasValue || x.Staff.Id == par.AccountId)
            && (
                (x.EndDate == default(DateTime) && par.IsVisibleNotFinish == true) ||
                (x.EndDate != default(DateTime) && par.IsVisibleFinish == true)
               )
            )
        {
        }
    }
}
