﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class SessionSpecParams
    {
        private const int MaxPageSize = 50;
        public int PageIndex { get; set; } = 1;
        private int _pageSize = 6;
        public int PageSize 
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
        public int? CompanyId { get; set; }
        public int? AccountId { get; set; }
        public string? Sort { get; set; }
        public bool? IsVisibleFinish { get; set; } = true;
        public bool? IsVisibleNotFinish { get; set; } = true;
    }
}
