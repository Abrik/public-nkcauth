﻿using Core.DTO.Account;
using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logic.ServiceInterfaces
{
    public interface IAccountService
    {
        Task<string> Login(AccountActionDto authDto);
        Task<string> Logout(AccountActionDto authDto);
        Task<IReadOnlyList<Staff>> GetAllGeneral();
    }
}
