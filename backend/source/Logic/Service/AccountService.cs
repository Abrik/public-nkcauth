﻿using Core.Const;
using Core.DTO.Account;
using Core.Entities;
using Core.Interfaces;
using Logic.ServiceInterfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Service
{
    public class AccountService : IAccountService
    {
        private readonly IGenericRepository<Staff> _repoAccount;
        private readonly IGenericRepository<Session> _repoSession;
        private int _staffId;
        private int _sessionId;

        public AccountService(
            IGenericRepository<Staff> repoAccount,
            IGenericRepository<Session> repoSession
            )
        {
            _repoAccount = repoAccount;
            _repoSession = repoSession;
        }

        public async Task<IReadOnlyList<Staff>> GetAllGeneral()
        {
            return await _repoAccount.GetListAsync();
        }

        public async Task<string> Login(AccountActionDto accountDto)
        {
            var valid = await BaseValid(accountDto, true);
            if (!string.IsNullOrWhiteSpace(valid))
                return valid;     

            var newSession = new Session()
            {
                CompanyId = accountDto.CompanyId,
                StaffId = _staffId,
                StartDate = DateTime.Now
            };
            _repoSession.Update(newSession);

            return "";
        }

        public async Task<string> Logout(AccountActionDto accountDto)
        {
            var valid = await BaseValid(accountDto, false);
            if (!string.IsNullOrWhiteSpace(valid))
                return valid;
            
            var session = await _repoSession.GetByIdAsync(_sessionId);
            session.EndDate = DateTime.Now;
            _repoSession.Update(session);

            return "";
        }

        private async Task<string> BaseValid(AccountActionDto authDto, bool isEnter)
        {
            if (authDto == null)
                return ErrorResponceMessage.EmptyDto;

            var staff = await GetStaffForEmail(authDto.Email);
            if (staff == null)
                return ErrorResponceMessage.StaffEmailNotFound;
            _staffId = staff.Id;

            var company = staff.Companies.FirstOrDefault(x => x.Id == authDto.CompanyId);
            if (company == null)
                return ErrorResponceMessage.StaffNotInCompany;

            var session = await _repoSession.GetPredicateFirstAsync(x =>
                        x.CompanyId == authDto.CompanyId
                        && x.StaffId == staff.Id
                        && x.EndDate == default(DateTime));
            _sessionId = session?.Id ?? 0;

            if (isEnter && session != null)
                return ErrorResponceMessage.SessionNotEmpty;

            if (!isEnter && session == null)
                return ErrorResponceMessage.SessionEmpty;

            return "";
        }

        private async Task<Staff> GetStaffForEmail(string email)
        {
            var staffs = await _repoAccount.GetWhithIncluseAsync(x => x.Email == email, y=>y.Companies);
            return staffs.FirstOrDefault();
        }
    }
}
