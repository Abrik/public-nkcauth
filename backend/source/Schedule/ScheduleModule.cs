﻿using Autofac;
using Autofac.Extras.Quartz;

namespace Schedule
{
    public class ScheduleModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new QuartzAutofacFactoryModule());
            builder.RegisterModule(new QuartzAutofacJobsModule(typeof(ScheduleModule).Assembly));
        }
    }
}
