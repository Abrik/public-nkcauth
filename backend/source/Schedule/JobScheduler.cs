﻿using Quartz;
using Quartz.Impl;
using Schedule.Jobs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Schedule
{
    public class JobScheduler
    {
        private readonly IScheduler _scheduler;

        public JobScheduler(IScheduler scheduler)
        {
            _scheduler = scheduler;
        }

        public async Task Start()
        {
            var reportJob = JobBuilder.Create<ReportJob>().Build();
            var reportTrigger = TriggerBuilder.Create()
                .WithIdentity(nameof(ReportJob), "jobs")
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(5)
                    .RepeatForever())
                .Build();
            await _scheduler.ScheduleJob(reportJob, reportTrigger);

            await _scheduler.Start();
        }
    }
}
