﻿using FirebirdSql.Data.FirebirdClient;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Schedule.Helpers
{
    public static class JobWorkerHelper
    {
        public static void CheckOrAlterProcedure(AuthContext _context, string sqlCheck, string sqlCreate)
        {
            var connection = (FbConnection)_context.Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
                connection.Open();

            var cmd = new FbCommand(sqlCheck, connection);
            var reader = cmd.ExecuteReader();
            var tryRead = reader.Read();
            if (!reader.HasRows || !tryRead)
            {
                _context.Database.ExecuteSqlRaw(sqlCreate);
            };
        }

        public static FbDataReader GetDataReader(AuthContext _context, string sql)
        {
            var connection = (FbConnection)_context.Database.GetDbConnection();
            var cmd = new FbCommand(sql, connection);
            return cmd.ExecuteReader();
        }
    }
}
