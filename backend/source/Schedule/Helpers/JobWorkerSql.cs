﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Helpers
{
    public static class JobWorkerSql
    {
        //------------------------------------------------------------
        public static string sql_check_SPR_SESSION_REPORTFULL = 
            GetSqlCheckProcedure("spr_session_reportfull");

        public const string sql_select_SPR_SESSION_REPORTFULL =
            "select * from spr_session_reportfull ";

        public const string sql_create_SPR_SESSION_REPORTFULL =
                " create or alter procedure spr_session_reportfull \n"+
                " returns( \n" +
                "     sessionid integer, \n" +
                "     companyname varchar(30), \n" +
                "     stafffullname varchar(90), \n" +
                "     sessionstart timestamp, \n" +
                "     sessionend timestamp, \n" +
                "     sessionduration double precision) \n" +
                " as \n" +
                " begin \n" +
                " for  select \n" +
                 " s.\"Id\", c.\"Name\", st.\"SurName\" ||' '||st.\"Name\"||' '||st.\"MiddleName\", \n"+
                " s.\"StartDate\", s.\"EndDate\", s.\"EndDate\" - s.\"StartDate\" \n"+
                " from \"Sessions\" s \n"+
                " left join \"Companies\" c on c.\"Id\" = s.\"CompanyId\" \n"+
                " left join \"Staffs\" st on st.\"Id\" = s.\"StaffId\" \n"+
                " where s.\"EndDate\" != '01.01.0001  0:00:00' \n"+
                " into :sessionid, :companyname, :stafffullname, :sessionstart, :sessionend, :sessionduration \n"+
                " do begin suspend; end end; \n";

        //------------------------------------------------------------
        public static string sql_check_SPR_SESSION_REPORTGROUP =
            GetSqlCheckProcedure("spr_session_reportgroup");

        public const string sql_select_SPR_SESSION_REPORTGROUP =
            "select * from spr_session_reportgroup";

        public const string sql_create_SPR_SESSION_REPORTGROUP =
                " create or alter procedure spr_session_reportgroup \n" +
                " returns( \n" +
                "     companyname varchar(30), \n" +
                "     stafffullname varchar(90), \n" +
                "     sessionduration double precision) \n" +
                " as \n" +
                " begin \n" +
                " for \n" +
                "   select s.companyname, s.stafffullname, sum(s.sessionduration) \n" +
                "   from spr_session_reportfull s \n" +
                "   group by 1,2 \n" +
                "   order by 1,2 \n" +
                " into :companyname, :stafffullname, :sessionduration \n" +
                " do begin suspend; end end \n";

        //------------------------------------------------------------
        private static string GetSqlCheckProcedure(string procName)
        {
            return $"SELECT 1 FROM RDB$PROCEDURES WHERE RDB$PROCEDURE_NAME = '{procName}'";
        }
    }
}
