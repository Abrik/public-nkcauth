﻿using Core.DTO.Report;
using Core.DTO.Session;
using Core.Entities;
using Core.Interfaces;
using FirebirdSql.Data.FirebirdClient;
using Infrastructure.Data;
using Logic.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quartz;
using Schedule.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Schedule.Jobs
{
    [DisallowConcurrentExecution]
    public class ReportJob : IJob
    {        
        private readonly IGenericRepository<Report> _reportRepo;
        private readonly AuthContext _context;

        public ReportJob(
             IGenericRepository<Report> reportRepo,
             AuthContext context
            )
        {
            _reportRepo = reportRepo;
            _context = context;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                JobWorkerHelper.CheckOrAlterProcedure(
                    _context,
                    JobWorkerSql.sql_check_SPR_SESSION_REPORTFULL,
                    JobWorkerSql.sql_create_SPR_SESSION_REPORTFULL
                    );

                JobWorkerHelper.CheckOrAlterProcedure(
                    _context,
                    JobWorkerSql.sql_check_SPR_SESSION_REPORTGROUP,
                    JobWorkerSql.sql_create_SPR_SESSION_REPORTGROUP
                    );

                var listFull = await GetFullList(_context, JobWorkerSql.sql_select_SPR_SESSION_REPORTFULL);
                var listGroup = await GetGroupList(_context, JobWorkerSql.sql_select_SPR_SESSION_REPORTGROUP);

                var jsonData = new JObject();
                jsonData.Add("dataDetail", JsonConvert.SerializeObject(listFull));
                jsonData.Add("dataGroup", JsonConvert.SerializeObject(listGroup));

                var report = new Report()
                {
                    CreationDate = DateTime.Now,
                    Data = Encoding.GetEncoding(1251).GetBytes(jsonData.ToString())
                };

                await _reportRepo.AddAsync(report);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private async Task<List<ReportfullDto>> GetFullList(AuthContext _context, string sql)
        {
            return await Task.Run(() =>
            {
                List<ReportfullDto> list = null;
                var reader = JobWorkerHelper.GetDataReader(_context, sql);
                if (reader.HasRows)
                {
                    list = new List<ReportfullDto>();
                    while (reader.Read())
                    {
                        var item = new ReportfullDto()
                        {
                            SessionId = reader.GetInt32(0),
                            CompanyName = reader.GetString(1),
                            StaffFullname = reader.GetString(2),
                            SessionStart = reader.GetDateTime(3),
                            SessionEnd = reader.GetDateTime(4),
                            SessionDuration = TimeSpan.FromDays(reader.GetDouble(5))
                        };
                        list.Add(item);
                    }
                }
                reader.Close();
                return list;
            });
        }

        private async Task<List<ReportgroupDto>> GetGroupList(AuthContext _context, string sql)
        {
            return await Task.Run(() =>
            {
                List<ReportgroupDto> list = null;
                var reader = JobWorkerHelper.GetDataReader(_context, sql);
                if (reader.HasRows)
                {
                    list = new List<ReportgroupDto>();
                    while (reader.Read())
                    {
                        var item = new ReportgroupDto()
                        {
                            CompanyName = reader.GetString(0),
                            StaffFullname = reader.GetString(1),
                            SessionDuration = TimeSpan.FromDays(reader.GetDouble(2))
                        };
                        list.Add(item);
                    }
                }
                reader.Close();
                return list;
            });
        }
    }
}
